# Connect Black - Desenvolvedor FullStack

Primeiramente, gostaríamos de agradecer por disponibilizar seu tempo em realizar o teste e pelo interesse em fazer parte da nossa equipe! \o

### Objetivos

O teste consiste em uma mini aplicação para controle de estoque. Neste cenário, iremos avaliar seu perfil técnico de back-end e front-end.

### Critérios de aceitação
- Deve criar migrations
- Deve ser uma API
- Deve construir o **front-end** com VueJS (preferencialmente) ou ReactJS
- Tests (PHPUnit) - não obrigatório mas seria interessante ;)

### Tecnologias
- PHP >= 7
- Laravel
- MySQL
- VueJS

### Funcionalidades
- Autenticação
- CRUD de Produtos
- Controle de Estoque

### Migrations
- Usuários
	nome, email, senha, data de cadastro
	
- Produtos
	nome, quantidade de estoque

## Prazo de entrega
Você possui um prazo total de 3 dias contando a partir da data do envio.

Após finalizar, crie um repositório no GitLab, BitBucket, GitHub ou em algum outro serviço e envie um email para joao@connectblack.com com o link do repositório.

Obrigado e boa sorte!
